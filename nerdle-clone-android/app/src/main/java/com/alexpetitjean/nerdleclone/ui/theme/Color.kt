package com.alexpetitjean.nerdleclone.ui.theme

import androidx.compose.ui.graphics.Color

val Beige = Color(0xFF989484)
val Green = Color(0xFF11690E)
val Purple = Color(0xFF8C2269)
val Steel = Color(0xFFE2E8F0)
