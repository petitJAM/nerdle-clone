package com.alexpetitjean.nerdleclone.ui.game

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.alexpetitjean.nerdleclone.game.GameState
import com.alexpetitjean.nerdleclone.game.isEquationBalanced
import com.alexpetitjean.nerdleclone.game.isEquationValid
import com.alexpetitjean.nerdleclone.ui.Route
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class GameViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
) : ViewModel() {

    private val equation: String = savedStateHandle[Route.Game.ARG_EQUATION]!!

    private val gameState = MutableStateFlow(GameState(equation, emptyList()))
    val uiGameState: Flow<UiGameState> = gameState.map(GameState::toUiGameState)

    val initialUiGameState = gameState.value.toUiGameState()

    fun makeGuess(guess: String): Boolean {
        Timber.d("Guess: $guess")
        if (guess.length < GameState.EQUATION_LENGTH) return false
        if (!isEquationValid(guess)) return false
        if (!isEquationBalanced(guess)) return false

        val currentGameState = gameState.value
        gameState.compareAndSet(currentGameState, currentGameState + guess)
        return true
    }
}
