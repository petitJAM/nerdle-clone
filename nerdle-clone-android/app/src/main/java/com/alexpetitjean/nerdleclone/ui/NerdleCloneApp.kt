package com.alexpetitjean.nerdleclone.ui

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.alexpetitjean.nerdleclone.ui.Route.Game.ARG_EQUATION
import com.alexpetitjean.nerdleclone.ui.game.GameScreen
import com.alexpetitjean.nerdleclone.ui.game.GameViewModel
import com.alexpetitjean.nerdleclone.ui.home.HomeScreen
import com.alexpetitjean.nerdleclone.ui.theme.NerdleCloneTheme

@Composable
fun NerdleCloneApp() {
    NerdleCloneTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colors.background
        ) {
            val navController = rememberNavController()
            NavHost(navController = navController, startDestination = Route.Home.route) {

                composable(Route.Home.route) {
                    // Generate equation from HomeViewModel?
                    HomeScreen(
                        onPlayClick = {
                            navController.navigate(Route.Game.createRoute("3*42=126"))
                        }
                    )
                }

                composable(
                    route = Route.Game.route,
                    arguments = listOf(
                        navArgument(ARG_EQUATION) { type = NavType.StringType }
                    ),
                ) {
                    val viewModel = hiltViewModel<GameViewModel>()
                    GameScreen(viewModel)
                }
            }
        }
    }
}

sealed class Route(
    val route: String,
) {

    object Home : Route("home")

    object Game : Route("game/{equation}") {
        const val ARG_EQUATION = "equation"
        fun createRoute(equation: String) = "game/$equation"
    }
}
