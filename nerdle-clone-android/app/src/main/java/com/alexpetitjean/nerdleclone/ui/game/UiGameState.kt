package com.alexpetitjean.nerdleclone.ui.game

import androidx.compose.ui.graphics.Color
import com.alexpetitjean.nerdleclone.game.Coloring
import com.alexpetitjean.nerdleclone.game.GameState
import com.alexpetitjean.nerdleclone.game.computeColorings
import com.alexpetitjean.nerdleclone.ui.theme.Beige
import com.alexpetitjean.nerdleclone.ui.theme.Green
import com.alexpetitjean.nerdleclone.ui.theme.Purple

data class UiGameState(
    val solution: String,
    val uiGuesses: List<UiGuess>,
) {
    val maxGuesses: Int = GameState.MAX_GUESSES
    val equationLength: Int = GameState.EQUATION_LENGTH
}

data class UiGuess(
    val colorings: List<Pair<Char, Color>>
) {
    companion object {
        val BLANK = UiGuess(
            listOf(
                ' ' to Beige,
                ' ' to Beige,
                ' ' to Beige,
                ' ' to Beige,
                ' ' to Beige,
                ' ' to Beige,
                ' ' to Beige,
                ' ' to Beige,
            )
        )
    }
}

fun GameState.toUiGameState() =
    UiGameState(
        solution = solution,
        uiGuesses = guesses.map { it.toUiGuess(solution) }
    )

fun String.toUiGuess(solution: String) =
    computeColorings(this, solution)
        .map(Coloring::toColor)
        .let { colors ->
            UiGuess(
                colorings = this.toList().zip(colors)
            )
        }

private fun Coloring.toColor() = when (this) {
    Coloring.CORRECT_POSITION -> Green
    Coloring.INCORRECT_POSITION -> Purple
    Coloring.NOT_IN_SOLUTION -> Color.Black
}
