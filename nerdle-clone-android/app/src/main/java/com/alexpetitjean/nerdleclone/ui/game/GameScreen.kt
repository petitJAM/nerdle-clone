package com.alexpetitjean.nerdleclone.ui.game

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.alexpetitjean.nerdleclone.ui.theme.Beige
import timber.log.Timber

@Composable
fun GameScreen(viewModel: GameViewModel) {
    val uiGameState by viewModel.uiGameState.collectAsState(initial = viewModel.initialUiGameState)

    // TODO: Move to ViewModel?
    var currentInput by remember { mutableStateOf("") }

    GameScreenContent(
        uiGameState = uiGameState,
        currentInput = currentInput,
        onCharacterKeyClicked = {
            if (currentInput.length < uiGameState.equationLength) {
                currentInput += it
            }
        },
        onEnterClicked = {
            if (viewModel.makeGuess(currentInput)) {
                currentInput = ""
            }
        },
        onDeleteClicked = {
            if (currentInput.isNotEmpty()) {
                currentInput = currentInput.dropLast(1)
            }
        }
    )
}

@Composable
private fun GameScreenContent(
    uiGameState: UiGameState,
    currentInput: String,
    onCharacterKeyClicked: (Char) -> Unit,
    onEnterClicked: () -> Unit,
    onDeleteClicked: () -> Unit,
) {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        TileGrid(
            uiGameState = uiGameState,
            currentInput = currentInput,
            modifier = Modifier
                .weight(0.5f)
        )

        Keyboard(
            onKeyClick = { key ->
                Timber.d("Clicked $key")
                when (key) {
                    is Key.Enter -> onEnterClicked()
                    is Key.Delete -> onDeleteClicked()
                    is Key.Character -> onCharacterKeyClicked(key.value)
                }
            },
            modifier = Modifier
                .padding(all = 24.dp)
                .weight(0.5f),
        )
    }
}

// region TileGrid

@Composable
private fun TileGrid(
    uiGameState: UiGameState,
    currentInput: String,
    modifier: Modifier = Modifier,
) {
    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.Center,
    ) {
        uiGameState.uiGuesses.forEach {
            TileRow(it)
            TileSpacer()
        }

        if (uiGameState.uiGuesses.size < uiGameState.maxGuesses) {
            val paddedCurrentInput = currentInput.padEnd(uiGameState.equationLength)
            TileRow(
                UiGuess(colorings = paddedCurrentInput.map { it to Beige })
            )
            TileSpacer()
        }

        if (uiGameState.uiGuesses.size < uiGameState.maxGuesses - 1) {
            repeat(uiGameState.maxGuesses - uiGameState.uiGuesses.size - 1) {
                TileRow(uiGuess = UiGuess.BLANK)
                TileSpacer()
            }
        }
    }
}

@Composable
private fun TileRow(uiGuess: UiGuess) {
    Row {
        uiGuess.colorings.forEach { (char, color) ->
            Tile(char, color)
            TileSpacer()
        }
    }
}

@Composable
private fun TileSpacer() {
    Spacer(modifier = Modifier.size(4.dp))
}

@Composable
private fun Tile(
    symbol: Char,
    color: Color,
    modifier: Modifier = Modifier
) {
    Box(
        modifier = modifier
            .size(40.dp)
            .clip(RoundedCornerShape(8.dp))
            .background(color),
        contentAlignment = Alignment.Center,
    ) {
        Text(
            text = symbol.toString(),
            color = Color.White,
        )
    }
}

// endregion TileGrid

@Preview
@Composable
fun Preview_GameScreenContent() {
    val solution = "3*42=126"
    val gameState = UiGameState(
        solution = solution,
        uiGuesses = listOf(
            "7*6-4=38".toUiGuess(solution),
            "4*63=252".toUiGuess(solution),
            "6*22=132".toUiGuess(solution),
            "3*42=126".toUiGuess(solution),
        )
    )

    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colors.background,
    ) {
        GameScreenContent(
            gameState,
            "1+2=",
            onCharacterKeyClicked = {},
            onEnterClicked = {},
            onDeleteClicked = {},
        )
    }
}
