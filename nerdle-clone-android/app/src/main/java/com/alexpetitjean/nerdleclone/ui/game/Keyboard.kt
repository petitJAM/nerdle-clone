package com.alexpetitjean.nerdleclone.ui.game

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.alexpetitjean.nerdleclone.R

sealed class Key(val label: String) {
    data class Character(val value: Char) : Key(value.toString())

    object Enter : Key("Enter")
    object Delete : Key("Delete")
}

@Composable
fun Keyboard(
    onKeyClick: (key: Key) -> Unit,
    modifier: Modifier = Modifier,
) {
    Row(
        modifier = modifier.fillMaxSize()
    ) {
        listOf(
            listOf('/', '*', '-', '+', '=').map(Key::Character),
            listOf('7', '4', '1', '0').map(Key::Character),
            listOf('8', '5', '2').map(Key::Character) + Key.Delete,
            listOf('9', '6', '3').map(Key::Character) + Key.Enter,
        ).forEachIndexed { index, keyList ->
            Column(modifier = Modifier.weight(1f)) {
                keyList.forEach { key ->
                    Key(key, onKeyClick, Modifier.weight(1f))
                }
            }
            if (index == 0) {
                Divider(
                    modifier = Modifier
                        .width(1.dp)
                        .fillMaxHeight()
                )
            }
        }
    }
}

@Composable
private fun Key(
    key: Key,
    onKeyClick: (key: Key) -> Unit,
    modifier: Modifier = Modifier,
) {
    IconButton(
        modifier = modifier.fillMaxWidth(),
        onClick = { onKeyClick(key) },
    ) {
        Box(
            contentAlignment = Alignment.Center,
        ) {
            when (key) {
                is Key.Character -> {
                    Text(
                        text = key.label,
                        style = TextStyle(
                            fontFamily = FontFamily.Monospace,
                            fontSize = 28.sp,
                        ),
                    )
                }
                is Key.Enter -> {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                    ) {
                        Icon(
                            painter = painterResource(R.drawable.ic_outline_calculate_24),
                            contentDescription = key.label,
                        )
                        Text(
                            text = key.label,
                            fontSize = 20.sp,
                        )
                    }
                }
                is Key.Delete -> {
                    Icon(
                        painter = painterResource(R.drawable.ic_outline_backspace_24),
                        contentDescription = key.label,
                    )
                }
            }
        }
    }
}

@Preview
@Composable
fun Preview_Keyboard() {
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colors.background,
    ) {
        Box(
            contentAlignment = Alignment.BottomCenter,
        ) {
            Keyboard(
                onKeyClick = {},
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(0.4f)
                    .padding(horizontal = 32.dp)
            )
        }
    }
}
