package com.alexpetitjean.nerdleclone.game

import java.util.Stack

fun computeColorings(guess: String, solution: String) = buildList {
    val solutionChars = solution.toMutableList()
    guess.forEachIndexed { index, char ->
        if (!solution.contains(char)) {
            add(Coloring.NOT_IN_SOLUTION)
        } else if (char == solution[index]) {
            solutionChars.remove(char)
            add(Coloring.CORRECT_POSITION)
        } else {
            if (solutionChars.contains(char)) {
                // In solution, but wrong position
                solutionChars.remove(char)
                add(Coloring.INCORRECT_POSITION)
            } else {
                // In solution, but already accounted for with a previous char
                add(Coloring.NOT_IN_SOLUTION)
            }
        }
    }
}

enum class Coloring {
    CORRECT_POSITION,
    INCORRECT_POSITION,
    NOT_IN_SOLUTION,
}

/**
 * Check if a given [equation] is valid (but not necessarily balanced).
 *
 * @see isEquationBalanced
 * @param equation string to be tested
 * @return `true` if equation meets all criteria
 */
fun isEquationValid(equation: String): Boolean {
    if (!equation.contains('=')) return false
    val parts = equation.split('=')
    val lhs = parts[0]
    val rhs = parts[1]
    if (lhs.isEmpty()) return false
    if (rhs.isEmpty()) return false
    if (!lhs.contains(operatorRegex)) return false
    if (rhs.contains(operatorRegex)) return false
    if (lhs.contains(Regex("\\b0\\d+\\b"))) return false
    if (lhs.contains(Regex("$operatorRegex{2,}"))) return false
    if (lhs.contains(Regex("^$operatorRegex"))) return false
    if (lhs.contains(Regex("$operatorRegex\$"))) return false
    return true
}

/**
 * Check if a given [equation] string is balanced. Call [isEquationValid] first or this will throw
 * an exception.
 *
 * @see isEquationValid
 * @param equation
 * @return `true` if LHS == RHS
 */
fun isEquationBalanced(equation: String): Boolean {
    require(isEquationValid(equation)) { "equation is not valid" }
    val parts = equation.split('=')
    val lhs = parts[0]
    val rhs = parts[1]
    val lhsValue = evaluateExpression(lhs)
    return lhsValue.isInt() && lhsValue.toInt() == rhs.toInt()
}

private fun evaluateExpression(expression: String): Float {
    val tokens = splitExpression(expression).toMutableList()
    val outputQueue = ArrayDeque<Token>()
    val operatorStack = Stack<Token.Operator>()

    while (tokens.isNotEmpty()) {
        when (val nextToken = tokens.removeAt(0)) {
            is Token.Number -> outputQueue.add(nextToken)
            is Token.Operator -> {
                while (operatorStack.isNotEmpty() && operatorStack.peek().precedence >= nextToken.precedence) {
                    outputQueue.add(operatorStack.pop())
                }
                operatorStack.push(nextToken)
            }
        }
    }

    while (operatorStack.isNotEmpty()) {
        val nextOperator = operatorStack.pop()
        outputQueue.add(nextOperator)
    }

    val numberStack = Stack<Float>()
    while (outputQueue.isNotEmpty()) {
        when (val token = outputQueue.removeFirst()) {
            is Token.Number -> numberStack.push(token.value.toFloat())
            is Token.Operator -> {
                val y = numberStack.pop()
                val x = numberStack.pop()
                when (token) {
                    Token.Operator.Add -> numberStack.push(x + y)
                    Token.Operator.Subtract -> numberStack.push(x - y)
                    Token.Operator.Multiply -> numberStack.push(x * y)
                    Token.Operator.Divide -> numberStack.push(x / y)
                }
            }
        }
    }

    return numberStack.pop()
}

private fun splitExpression(expression: String): List<Token> =
    // Positive lookahead/lookbehind assert that we're before or after an operator
    expression.split(Regex("(?=$operatorRegex)|(?<=$operatorRegex)"))
        .map { tokenStr ->
            val number = tokenStr.toIntOrNull()
            if (number != null) {
                Token.Number(number)
            } else {
                when (tokenStr) {
                    "+" -> Token.Operator.Add
                    "-" -> Token.Operator.Subtract
                    "*" -> Token.Operator.Multiply
                    "/" -> Token.Operator.Divide
                    else -> throw UnknownTokenException(tokenStr)
                }
            }
        }

private val operatorRegex = Regex("[-+*/]")

private fun Float.isInt(): Boolean {
    return this - this.toInt() == 0f
}

private class UnknownTokenException(token: String) : Exception("Unknown token: $token")

private sealed class Token {
    data class Number(val value: Int) : Token()
    sealed class Operator(val precedence: Int) : Token() {
        object Add : Operator(1) {
            override fun toString() = "Operator(+)"
        }

        object Subtract : Operator(1) {
            override fun toString() = "Operator(-)"
        }

        object Multiply : Operator(2) {
            override fun toString() = "Operator(*)"
        }

        object Divide : Operator(2) {
            override fun toString() = "Operator(/)"
        }
    }
}
