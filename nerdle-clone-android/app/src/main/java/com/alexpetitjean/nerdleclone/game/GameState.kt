package com.alexpetitjean.nerdleclone.game

data class GameState(
    val solution: String,
    val guesses: List<String>,
) {

    val status: Status
        get() = when {
            guesses.last() == solution -> Status.WIN
            guesses.size >= MAX_GUESSES -> Status.LOSE
            else -> Status.INCOMPLETE
        }

    operator fun plus(guess: String): GameState {
        return copy(
            guesses = guesses + guess
        )
    }

    enum class Status {
        WIN, LOSE, INCOMPLETE,
    }

    companion object {
        const val MAX_GUESSES = 6
        const val EQUATION_LENGTH = 8
    }
}
