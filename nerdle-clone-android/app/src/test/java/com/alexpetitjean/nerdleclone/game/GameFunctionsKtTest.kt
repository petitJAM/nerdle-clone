package com.alexpetitjean.nerdleclone.game

import com.google.common.truth.Truth.assertThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

internal class GameFunctionsKtTest {

    @Nested
    @DisplayName("computeColorings")
    inner class ComputeColoringsTest {

        @Test
        fun `completely matching guess`() {
            assertThat(
                computeColorings("1+3=4", "1+3=4")
            ).isEqualTo(
                listOf(
                    Coloring.CORRECT_POSITION,
                    Coloring.CORRECT_POSITION,
                    Coloring.CORRECT_POSITION,
                    Coloring.CORRECT_POSITION,
                    Coloring.CORRECT_POSITION,
                )
            )
        }

        @Test
        fun `completely incorrect guess`() {
            assertThat(
                computeColorings("18-7-4=7", "3*9*2=56")
            ).isEqualTo(
                listOf(
                    Coloring.NOT_IN_SOLUTION,
                    Coloring.NOT_IN_SOLUTION,
                    Coloring.NOT_IN_SOLUTION,
                    Coloring.NOT_IN_SOLUTION,
                    Coloring.NOT_IN_SOLUTION,
                    Coloring.NOT_IN_SOLUTION,
                    Coloring.INCORRECT_POSITION,
                    Coloring.NOT_IN_SOLUTION,
                )
            )
        }

        @Test
        fun `double char in guess but only one in solution`() {
            assertThat(
                computeColorings("12+13=25", "10/2+2=7")
            ).isEqualTo(
                listOf(
                    Coloring.CORRECT_POSITION,
                    Coloring.INCORRECT_POSITION,
                    Coloring.INCORRECT_POSITION,
                    Coloring.NOT_IN_SOLUTION,
                    Coloring.NOT_IN_SOLUTION,
                    Coloring.INCORRECT_POSITION,
                    Coloring.INCORRECT_POSITION,
                    Coloring.NOT_IN_SOLUTION,
                )
            )
        }

        @Test
        fun `nerdle 2022-02-26 guess 1`() {
            assertThat(
                computeColorings("7*6-4=38", "3*42=126")
            ).isEqualTo(
                listOf(
                    Coloring.NOT_IN_SOLUTION,
                    Coloring.CORRECT_POSITION,
                    Coloring.INCORRECT_POSITION,
                    Coloring.NOT_IN_SOLUTION,
                    Coloring.INCORRECT_POSITION,
                    Coloring.INCORRECT_POSITION,
                    Coloring.INCORRECT_POSITION,
                    Coloring.NOT_IN_SOLUTION,
                )
            )
        }

        @Test
        fun `nerdle 2022-02-26 guess 2`() {
            assertThat(
                computeColorings("4*63=252", "3*42=126")
            ).isEqualTo(
                listOf(
                    Coloring.INCORRECT_POSITION,
                    Coloring.CORRECT_POSITION,
                    Coloring.INCORRECT_POSITION,
                    Coloring.INCORRECT_POSITION,
                    Coloring.CORRECT_POSITION,
                    Coloring.INCORRECT_POSITION,
                    Coloring.NOT_IN_SOLUTION,
                    Coloring.INCORRECT_POSITION,
                )
            )
        }

        @Test
        fun `nerdle 2022-02-26 guess 3`() {
            assertThat(
                computeColorings("6*22=132", "3*42=126")
            ).isEqualTo(
                listOf(
                    Coloring.INCORRECT_POSITION,
                    Coloring.CORRECT_POSITION,
                    Coloring.INCORRECT_POSITION,
                    Coloring.CORRECT_POSITION,
                    Coloring.CORRECT_POSITION,
                    Coloring.CORRECT_POSITION,
                    Coloring.INCORRECT_POSITION,
                    Coloring.NOT_IN_SOLUTION,
                )
            )
        }

        @Test
        fun `nerdle 2022-02-26 guess 4`() {
            assertThat(
                computeColorings("3*42=126", "3*42=126")
            ).isEqualTo(
                listOf(
                    Coloring.CORRECT_POSITION,
                    Coloring.CORRECT_POSITION,
                    Coloring.CORRECT_POSITION,
                    Coloring.CORRECT_POSITION,
                    Coloring.CORRECT_POSITION,
                    Coloring.CORRECT_POSITION,
                    Coloring.CORRECT_POSITION,
                    Coloring.CORRECT_POSITION,
                )
            )
        }
    }

    @Nested
    @DisplayName("isEquationValid")
    inner class IsEquationValidTest {

        @Test
        fun `valid equation 1`() {
            assertThat(isEquationValid("1+2=3")).isTrue()
        }

        @Test
        fun `valid equation 2`() {
            assertThat(isEquationValid("1+20/4*6=377")).isTrue()
        }

        @Test
        fun `valid equation 3`() {
            assertThat(isEquationValid("100-98*4/2+8+19=232")).isTrue()
        }

        @Test
        fun `no equals 1`() {
            assertThat(isEquationValid("134")).isFalse()
        }

        @Test
        fun `no equals 2`() {
            assertThat(isEquationValid("1+2+3")).isFalse()
        }

        @Test
        fun `no operators on lhs`() {
            assertThat(isEquationValid("100=100")).isFalse()
        }

        @Test
        fun `operators on rhs`() {
            assertThat(isEquationValid("1+3=2+2")).isFalse()
        }

        @Test
        fun `blank lhs`() {
            assertThat(isEquationValid("=100")).isFalse()
        }

        @Test
        fun `blank rhs`() {
            assertThat(isEquationValid("1+2+3=")).isFalse()
        }

        @Test
        fun `consecutive operators 1`() {
            assertThat(isEquationValid("1++2=4")).isFalse()
        }

        @Test
        fun `consecutive operators 2`() {
            assertThat(isEquationValid("1*/-+2=4")).isFalse()
        }

        @Test
        fun `leading operators`() {
            assertThat(isEquationValid("+1+50=51")).isFalse()
        }

        @Test
        fun `trailing operators`() {
            assertThat(isEquationValid("1+2+=3")).isFalse()
        }

        @Test
        fun `no leading zeros`() {
            assertThat(isEquationValid("01+2=3")).isFalse()
        }
    }

    @Nested
    @DisplayName("isEquationBalanced")
    inner class IsEquationBalancedTest {

        @Test
        fun `throws if equation has no equals`() {
            val e = assertThrows<IllegalArgumentException> { isEquationBalanced("1+2+3") }
            assertThat(e).hasMessageThat().isEqualTo("equation is not valid")
        }

        @Test
        fun `throws if equation has no operators on lhs`() {
            val e = assertThrows<IllegalArgumentException> { isEquationBalanced("100=100") }
            assertThat(e).hasMessageThat().isEqualTo("equation is not valid")
        }

        @Test
        fun `throws if equation has blank lhs`() {
            val e = assertThrows<IllegalArgumentException> { isEquationBalanced("=500") }
            assertThat(e).hasMessageThat().isEqualTo("equation is not valid")
        }

        @Test
        fun `throws if equation has blank rhs`() {
            val e = assertThrows<IllegalArgumentException> { isEquationBalanced("1+2+3=") }
            assertThat(e).hasMessageThat().isEqualTo("equation is not valid")
        }

        @Test
        fun `simple addition true`() {
            assertThat(isEquationBalanced("1+2+9=12")).isTrue()
        }

        @Test
        fun `simple addition false`() {
            assertThat(isEquationBalanced("1+2+3=50")).isFalse()
        }

        @Test
        fun `simple subtraction true`() {
            assertThat(isEquationBalanced("100-98=2")).isTrue()
        }

        @Test
        fun `simple subtraction false`() {
            assertThat(isEquationBalanced("100-98=8")).isFalse()
        }

        @Test
        fun `simple multiplication true`() {
            assertThat(isEquationBalanced("62*2=124")).isTrue()
        }

        @Test
        fun `simple multiplication false`() {
            assertThat(isEquationBalanced("1*2*3=50")).isFalse()
        }

        @Test
        fun `simple division true`() {
            assertThat(isEquationBalanced("20/2/2=5")).isTrue()
        }

        @Test
        fun `simple division false`() {
            assertThat(isEquationBalanced("100/40=7")).isFalse()
        }

        @Test
        fun `divide by zero false`() {
            assertThat(isEquationBalanced("100/0=10")).isFalse()
        }

        @Test
        fun `order of operations 1`() {
            assertThat(isEquationBalanced("2+5*3=17")).isTrue()
        }

        @Test
        fun `order of operations 2`() {
            assertThat(isEquationBalanced("2+5*3=21")).isFalse()
        }

        @Test
        fun `order of operations 3`() {
            assertThat(isEquationBalanced("2+10/2=7")).isTrue()
        }

        @Test
        fun `order of operations 4`() {
            assertThat(isEquationBalanced("2+10/2=6")).isFalse()
        }

        @Test
        fun `complex expression`() {
            assertThat(isEquationBalanced("2+3*4-5+7*6/3-2*3+5-2*2=18")).isTrue()
        }
    }
}
