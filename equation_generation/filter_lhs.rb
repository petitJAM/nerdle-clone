require_relative 'generated_files.rb'
require_relative 'parse_lhs.rb'

start = Time.now

File.open(FILTERED_LHS_FILE, 'w') do |file|
  File.foreach(ALL_LHS_FILE) do |lhs|
    parse_lhs lhs # raises if invalid
    file.puts lhs
  rescue ParseError
  end
end

finish = Time.now
diff = finish - start
puts "Filtered syntactically correct LHS's in #{diff.round(2)} seconds"
