# Generate everything

```sh
ruby generate_all_lhs.rb && ruby filter_lhs.rb && ruby generate_rhs.rb && ruby filter_interesting_equations.rb
```

# Interesting Stats

```sh
$ wc -l all_interesting_equations.txt all_len_8_equations.txt syntax_filtered_lhs.txt all_valid_and_invalid_lhs.txt

   17723 all_interesting_equations.txt
   67346 all_len_8_equations.txt
 1927600 syntax_filtered_lhs.txt
 8105776 all_valid_and_invalid_lhs.txt
```
