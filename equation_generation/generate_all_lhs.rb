=begin

Rules:
- Balanced equation
- Right side is only digits
- No negative numbers
- No leading zeros

Equals in index 0 or 7 is obviously not good.

X=XXXXXX can't possibly work.
XX=XXXXX can't either.

XXX=XXXX can at best be X*X=XXXX, but no numbers can satisfy that.

XXXX=XXX can be e.g., 99+1=100 or 99*2=198, so this spot is fine.

XXXXX=XX
XXXXXX=X can both work fine as well.

Three possible indexes for the equals:
4, 5, 6

Total permutations with repetition that _might_ be balanced equations:

  14^4*10^3 + 14^5*10^2 + 14^6*10 = 167493760

14 -> non-equals digits/symbols
10 -> digits

Some of those will have leading symbols or leading zeros, which is not allowed.
Some of those will be valid syntax, but imbalanced.

---

Upon further thinking, I've realized that there's no reason to generate the right hand side of the equation since I could instead just calculate it.

=end

require_relative 'generated_files.rb'

start = Time.now

digits = [
  '0',
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
]

operators = [
  '+',
  '-',
  '*',
  '/',
]

chars = digits + operators

File.open(ALL_LHS_FILE, 'w') do |file|
  chars.repeated_permutation(4).flat_map do |lhs|
    file.puts lhs.join
  end

  chars.repeated_permutation(5).flat_map do |lhs|
    file.puts lhs.join
  end

  chars.repeated_permutation(6).flat_map do |lhs|
    file.puts lhs.join
  end
end

finish = Time.now

diff = finish - start
puts "Generated all possible LHS's in #{diff.round(2)} seconds"

=begin
# Faster version for testing
file_name = 'all_valid_and_invalid_lhs.txt'
test_out_file = File.join(File.dirname(__FILE__), 'all_valid_and_invalid_lhs.txt')

File.open(test_out_file, 'w') do |file|
  chars.repeated_permutation(3).flat_map do |lhs|
    digits.repeated_permutation(1).map do |rhs|
      file.puts (lhs + ['='] + rhs).join
    end
  end
end
=end
