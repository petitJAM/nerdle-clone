require_relative 'generated_files.rb'

start = Time.now

File.open(ALL_INTERESTING_EQUATIONS_FILE, 'w') do |file|
  File.foreach(ALL_LEN_8_EQUATIONS_FILE, chomp: true) do |equation|
    lhs, rhs = equation.split '='
    next if lhs =~ /\b0\b/
    file.puts equation
  end
end

finish = Time.now
diff = finish - start
puts "Filtered interesting equations in #{diff.round(2)} seconds"
