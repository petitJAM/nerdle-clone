require 'test/unit'

require_relative 'parse_lhs.rb'
require_relative 'evaluate_lhs.rb'

class TestEvaluateLhs < Test::Unit::TestCase

  def test_empty_string
    assert_raises(EmptyStringError) { evaluate_lhs '' }
  end

  def test_raise_parse_error
    assert_kind_of(ParseError, assert_raises { evaluate_lhs '+1+2-3' })
    assert_kind_of(ParseError, assert_raises { evaluate_lhs '12***3' })
    assert_kind_of(ParseError, assert_raises { evaluate_lhs '01-2+4' })
    assert_kind_of(ParseError, assert_raises { evaluate_lhs '1+2+' })
    assert_kind_of(ParseError, assert_raises { evaluate_lhs '1234' })
  end

  def test_division_not_integer
    assert_raises(NotIntegerError) { evaluate_lhs '55/2' }
    assert_raises(NotIntegerError) { evaluate_lhs '4+83/2' }
    assert_raises(NotIntegerError) { evaluate_lhs '3-4+83/2' }
    assert_raises(NotIntegerError) { evaluate_lhs '4*17-4+83/2' }
  end

  def test_simple_addition
    assert_equal(
      3,
      evaluate_lhs('1+2')
    )

    assert_equal(
      1344,
      evaluate_lhs('999+345')
    )

    assert_equal(
      6,
      evaluate_lhs('1+2+3')
    )
  end

  def test_simple_subtraction
    assert_equal(
      6,
      evaluate_lhs('9-3')
    )

    assert_equal(
      4222,
      evaluate_lhs('4567-345')
    )

    assert_equal(
      4,
      evaluate_lhs('9-2-3')
    )
  end

  def test_simple_multiplication
    assert_equal(
      40,
      evaluate_lhs('8*5')
    )

    assert_equal(
      1776,
      evaluate_lhs('48*37')
    )

    assert_equal(
      54,
      evaluate_lhs('9*2*3')
    )
  end

  def test_simple_division
    assert_equal(
      4,
      evaluate_lhs('8/2')
    )

    assert_equal(
      6,
      evaluate_lhs('48/8')
    )

    assert_equal(
      46,
      evaluate_lhs('4830/15/7')
    )
  end

  def test_addition_and_subtraction
    assert_equal(
      16,
      evaluate_lhs('10+8-2')
    )

    assert_equal(
      589,
      evaluate_lhs('546+45-2')
    )
  end

  def test_multiplication_and_division
    assert_equal(
      32,
      evaluate_lhs('20/5*8')
    )

    assert_equal(
      1518,
      evaluate_lhs('345/5*22')
    )
  end

  def test_addition_and_multiplication
    assert_equal(
      19,
      evaluate_lhs('4+5*3')
    )

    assert_equal(
      596,
      evaluate_lhs('428+6*28')
    )
  end

  def test_complex
    assert_equal(
      38,
      evaluate_lhs('76/2+5-30/6')
    )

    assert_equal(
      311,
      evaluate_lhs('375+46-22*55/11')
    )
  end

  def test_negative_result
    assert_raises(NegativeResultError) { evaluate_lhs('4-10') }
    assert_raises(NegativeResultError) { evaluate_lhs('1-45*34') }
    assert_raises(NegativeResultError) { evaluate_lhs('100-1000/2') }
  end

end
