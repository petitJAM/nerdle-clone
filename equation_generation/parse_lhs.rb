class ParseError < StandardError; end

class EmptyStringError < ParseError; end
class RepeatedOperatorError < ParseError; end
class LeadingOperatorError < ParseError; end
class TrailingOperatorError < ParseError; end
class LeadingZerosError < ParseError; end
class NoOperatorsError < ParseError; end

def parse_lhs(lhs)
  raise EmptyStringError if lhs.empty?

  operator_regex = '[+\-*\/]'
  lhs_parts = lhs.split /\b/

  raise RepeatedOperatorError if lhs_parts.any? /#{operator_regex}{2,}/
  raise LeadingOperatorError if lhs_parts.first =~ /#{operator_regex}/
  raise TrailingOperatorError if lhs_parts.last =~ /#{operator_regex}/
  raise LeadingZerosError if lhs_parts.any? /^0\d+/
  raise NoOperatorsError if lhs_parts.none? /#{operator_regex}/

  lhs_parts
end
