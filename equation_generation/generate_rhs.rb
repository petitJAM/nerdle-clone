require_relative 'generated_files.rb'
require_relative 'evaluate_lhs.rb'

start = Time.now

File.open(ALL_LEN_8_EQUATIONS_FILE, 'w') do |file|
  File.foreach(FILTERED_LHS_FILE, chomp: true) do |lhs|
    rhs = evaluate_lhs lhs
    combined = lhs + '=' + rhs.to_s
    file.puts combined if combined.size == 8

  rescue EvaluateError, ParseError
  end
end

finish = Time.now
diff = finish - start
puts "Generated all length 8 equations in #{diff.round(2)} seconds"
