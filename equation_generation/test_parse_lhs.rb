require 'test/unit'

require_relative 'parse_lhs.rb'

class TestParseLhs < Test::Unit::TestCase

  def test_empty_string
    assert_raises(EmptyStringError) { parse_lhs('') }
  end

  def test_single_digits
    assert_equal(
      ['1', '+', '2', '+', '3'],
      parse_lhs('1+2+3')
    )
  end

  def test_two_digits
    assert_equal(
      ['12', '+', '3'],
      parse_lhs('12+3')
    )
  end

  def test_repeated_operators
    assert_raises(RepeatedOperatorError) { parse_lhs('*12+-3') }
    assert_raises(RepeatedOperatorError) { parse_lhs('1+*-/2') }
  end

  def test_star_only
    assert_raises(RepeatedOperatorError) { parse_lhs('12****3') }
  end

  def test_starts_with_operator
    assert_raises(LeadingOperatorError) { parse_lhs('+123-1') }
    assert_raises(LeadingOperatorError) { parse_lhs('-123+1') }
  end

  def test_no_leading_zeros
    assert_raises(LeadingZerosError) { parse_lhs('01+2+3') }
    assert_raises(LeadingZerosError) { parse_lhs('1+02+3') }
    assert_raises(LeadingZerosError) { parse_lhs('1+00002+3') }
    assert_raises(LeadingZerosError) { parse_lhs('000001+2+3') }
  end

  def test_no_trailing_operator_in_lhs
    assert_raises(TrailingOperatorError) { parse_lhs('1+2+') }
    assert_raises(TrailingOperatorError) { parse_lhs('1+2-') }
    assert_raises(TrailingOperatorError) { parse_lhs('1+2*') }
    assert_raises(TrailingOperatorError) { parse_lhs('1+2/') }
  end

  def test_at_least_one_operator_lhs
    assert_raises(NoOperatorsError) {refute parse_lhs('12345') }
    assert_raises(NoOperatorsError) { parse_lhs('67890') }
  end

  def test_numbers_with_zeros
    assert_equal(
      ['100', '-', '1000', '/', '2'],
      parse_lhs('100-1000/2')
    )

    assert_equal(
      ['10001', '*', '2', '-', '4'],
      parse_lhs('10001*2-4')
    )
  end

end
