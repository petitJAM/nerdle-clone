ALL_LHS_FILE = File.join File.dirname(__FILE__), 'all_valid_and_invalid_lhs.txt'
FILTERED_LHS_FILE = File.join File.dirname(__FILE__), 'syntax_filtered_lhs.txt'
ALL_LEN_8_EQUATIONS_FILE = File.join File.dirname(__FILE__), 'all_len_8_equations.txt'
ALL_INTERESTING_EQUATIONS_FILE = File.join File.dirname(__FILE__), 'all_interesting_equations.txt'
