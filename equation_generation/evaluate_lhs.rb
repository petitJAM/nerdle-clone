require_relative 'parse_lhs.rb'

class EvaluateError < StandardError; end

class NotIntegerError < EvaluateError; end
class NegativeResultError < EvaluateError; end

def evaluate_lhs(str)
  lhs_parts = parse_lhs(str)

  str_to_f = str.gsub(/(\d+)/, '\1.to_f')
  rhs = eval str_to_f

  raise NotIntegerError unless rhs % 1 == 0
  raise NegativeResultError if rhs.to_i < 0

  rhs.to_i
end
